#!/usr/bin/env bash

# Check for Oh My Zsh and install if we don't have it
if [ ! -d $HOME/.oh-my-zsh ]; then
  info "Installing Oh My Zsh"
  /bin/sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/HEAD/tools/install.sh)"
  success "Oh My Zsh installed"
fi

# Check for zsh-autosuggestions and install if we don't have it
if [ ! -d $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions ]; then
  info "Installing zsh-autosuggestions"
  git clone https://github.com/zsh-users/zsh-autosuggestions $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions
  success "Autosuggestions installed"
else
  info "Updating zsh-autosuggestions"
  git -C $HOME/.oh-my-zsh/custom/plugins/zsh-autosuggestions pull
  success "Autosuggestions updated"
fi

# Check for powerlevel10k and install if we don't have it
if [ ! -d $HOME/.oh-my-zsh/custom/themes/powerlevel10k ]; then
  info "Installing powerlevel10k"
  git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $HOME/.oh-my-zsh/custom/themes/powerlevel10k
  success "powerlevel10k installed"
else
  info "Updating powerlevel10k"
  git -C $HOME/.oh-my-zsh/custom/themes/powerlevel10k pull
  success "powerlevel10k updated"
fi
