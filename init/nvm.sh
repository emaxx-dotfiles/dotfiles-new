#!/usr/bin/env bash

if [[ ! -d $HOME/.nvm ]]; then
  info "Installing nvm"
  git clone https://github.com/nvm-sh/nvm.git $HOME/.nvm
  \. "$HOME/.nvm/nvm.sh"  # This loads nvm
  nvm install node
  sucess "nvm installed"
fi
