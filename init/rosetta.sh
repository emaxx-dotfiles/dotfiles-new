#!/usr/bin/env bash

UNAME_MACHINE="$(/usr/bin/uname -m)"
if [[ "$UNAME_MACHINE" == "arm64" ]]; then
    if [[ ! -f "/Library/Apple/System/Library/LaunchDaemons/com.apple.oahd.plist" ]]; then
      info "Installing Rosetta"
      sudo softwareupdate --install-rosetta --agree-to-license
      success "Rosetta installed"
    else
      info "Rosetta already installed"
    fi
fi
