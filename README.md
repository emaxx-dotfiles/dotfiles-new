## Introduction

This repository provides both a setup and dotfiles configuration for MacOS. It should remove the difficulty inherent in finding all the new software that needs setting up on a new devices, and provides a set of (in my opinion) sane defaults for applications.

## Fresh Setup

For a new device, you can follow the below instructions to both install software and dotfiles.

### Set up your MacOS Device

Ensure you've backed up any existing configuration files that you wish to keep as we do not guarantee a back up of files as part of this repo.

1. Update MacOS to the latest version.
2. Generate a new public and private SSH key by runnninng:

``` zsh
curl https://raw.githubusercontent.com/emaxxxed/dots/HEAD/init/ssh.sh
```

3. Clone this repo to `~/.dots` with:

``` zsh
git clone --recursive https://github.com/emaxxxed/dots.git ~/.dots
```

4. Run the installation with:

``` zsh
~/.dots/init.sh
```

The defaults should now be set, software installed, and you should be ready to go!

This script will set up an auto-updater, which should pull this repo semi-regularly and integrate any changes, as well as ensuring brew and the like is kept up to date.

## Customising these Dots

