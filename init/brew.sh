#!/usr/bin/env bash


# Check for Homebrew and install if we don't have it
if ! command -v brew &> /dev/null; then
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

  UNAME_MACHINE="$(/usr/bin/uname -m)"

  info "Installing Homebrew"

  if [[ "$UNAME_MACHINE" == "arm64" ]]; then
    # On ARM macOS, homebrew installs to /opt/homebrew only
    echo 'eval $(/opt/homebrew/bin/brew shellenv)' >> $HOME/.zprofile_local
    eval $(/opt/homebrew/bin/brew shellenv)
    success "Homebrew installed"
  else
    # On Intel macOS, homebrew installs to /usr/local only
    eval $(/usr/local/bin/brew shellenv)
  fi
  success "Homebrew installed"
fi
