#!/bin/sh

DOTFILES="${DOTFILES:-$HOME/.dots}"
set -e

info() {
	# shellcheck disable=SC2059
	printf "\r  [ \033[00;34m..\033[0m ] $1\n"
}

user() {
	# shellcheck disable=SC2059
	printf "\r  [ \033[0;33m??\033[0m ] $1\n"
}

success() {
	# shellcheck disable=SC2059
	printf "\r\033[2K  [ \033[00;32mOK\033[0m ] $1\n"
}

fail() {
	# shellcheck disable=SC2059
	printf "\r\033[2K  [\033[0;31mFAIL\033[0m] $1\n"
	echo ''
	exit
}

info "Beginning Setup"

# install SSH
source ./init/ssh.sh

# Install git
source ./init/git.sh

# Install nvm and a default version of node.
source ./init/nvm.sh

#Install Rosetta, just in case the user requires it.
source ./init/rosetta.sh

# Install ohmyzsh and the like
source ./init/shell.sh

#install dotfiles...
source ./init/dotfiles.sh

#Install software
source ./init/software.sh
