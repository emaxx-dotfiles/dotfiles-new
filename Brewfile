#!/usr/bin/env ruby

# Taps
tap 'homebrew/cask'
tap 'homebrew/cask-fonts'
tap 'homebrew/cask-versions'
tap 'homebrew/bundle'
tap 'nicoverbruggen/homebrew-cask'
tap 'd12frosted/emacs-plus'

# Binaries
brew 'awscli'
# brew 'bash' # Latest Bash version
brew 'coreutils' # Those that come with macOS are outdated
brew 'ffmpeg'
brew 'gh'
brew 'git'
brew 'grep'
brew 'httpie'
brew 'jq' # Used for spatie/visit
brew 'mackup'
brew 'mas' # Mac App Store manager
brew 'pkg-config' # https://github.com/driesvints/dotfiles/issues/20
brew 'stow' # dotfile management
brew 'svn' # Needed to install fonts

# Spatie Medialibrary
brew 'jpegoptim'
brew 'optipng'
brew 'pngquant'
brew 'svgo'
brew 'gifsicle'

# Development
brew 'imagemagick'

# Apps
cask '1password' # Password Manager
cask 'alfred' # Better spotlight
cask 'caffeine' # Prevent system sleep
cask 'discord' # Conversations with Friends
cask 'docker' # Like your machine, but portable
cask 'emacs-plus' # The best editor
cask 'fig' # Terminal autocomplete goodness
cask 'figma' # Designs are your friend
cask 'firefox' # The best browser
cask 'github' # Open-sourcium
cask 'google-chrome' # The other browser
cask 'hyper' # Web-based terminal...
cask 'imageoptim' # Optimize your images
cask 'insomnia' # Test those APIs properly.
cask 'neovim' # :wqqqqqq!!ZZZZZZ!:wqesc:WQ
cask 'pastebot' # A decent clipboard
cask 'screenflow' # Record your screen, hide the weird tabs
cask 'slack' # Mostly for memes
cask 'spotify' # Drown out coworkers
cask 'the-unarchiver' # no more tar -xzvf
cask 'visual-studio-code' # The other editor
cask 'whatsapp' # Chat with people!
cask 'zoom' # It's not teams.

# Quicklook
cask 'qlmarkdown' # quicklook markdown files in finder
cask 'quicklook-json' # quicklook json files

# Fonts
cask 'font-lato'
cask 'font-open-sans'
cask 'font-roboto'
cask 'font-source-code-pro-for-powerline'
cask 'font-source-code-pro'
cask 'font-source-sans-pro'
cask 'font-source-serif-pro'

# Mac App Store
mas 'Giphy Capture', id: 668208984 # like a screen recorder but with gifs
mas 'Speedtest', id: 1153157709 # for when your inter(...)
mas 'Tweetbot', id: 1384080005 # fit your thoughts into ~140~ 280 characters or less
