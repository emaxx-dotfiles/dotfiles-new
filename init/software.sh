#!/usr/bin/env bash
set -e
DOTFILES="${DOTFILES:-$HOME/.dots}"

info "Installing brew packages from Brewfile"

brew update
brew tap homebrew/bundle
brew bundle --file $DOTFILES/Brewfile
brew cleanup

success "Brew packages installed"
